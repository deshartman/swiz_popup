package event
{
	import flash.display.DisplayObject;
	import flash.events.Event;

	public class FireEvent extends Event
	{
		public static const FIRE_EVENT:String = "FireEvent";
		
		public var viewParent:DisplayObject;
	
		public function FireEvent(type:String, bubbles:Boolean = true, cancelable:Boolean = false) {
			super(type, bubbles, cancelable);
		}
	}
}