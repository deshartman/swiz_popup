package event
{
	import flash.events.Event;

	public class NavigationEvent extends Event
	{
		public static const OPEN_POPUP:String = "OpenPopUp";
		public static const CLOSE_POPUP:String = "ClosePopUp";
		
		public var message:String;
		public var showAgain:Boolean;
		public var accept:Boolean;
		
		public function NavigationEvent(type:String, bubbles:Boolean = true, cancelable:Boolean = false) {
			super(type, bubbles, cancelable);
		}

	}
}