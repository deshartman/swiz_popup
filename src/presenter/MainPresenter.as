package presenter
{
	import event.FireEvent;
	import event.NavigationEvent;
	
	import flash.display.DisplayObject;
	import flash.events.EventDispatcher;
	import flash.events.IEventDispatcher;
	
	import mx.managers.PopUpManager;
	
	import view.popup.MyPopUp;

	[Bindable]
	public class MainPresenter
	{
		[Dispatcher] public var eventDispatcher:EventDispatcher;
		
		public var popUpClassProp:Class;
		
		
		[Mediate(event="NavigationEvent.OPEN_POPUP")]
		public function showMainPopUp(navigationEvent:NavigationEvent):void
		{
			trace("Mediated Open Popup Event");
			popUpClassProp = MyPopUp;
		}
		
		public function callEvent():void
		{
			trace("Disatching Controller Event");
			//popUpClassProp = MyPopUp;
			var fireEvent:FireEvent = new FireEvent(FireEvent.FIRE_EVENT);
			eventDispatcher.dispatchEvent(fireEvent);
		}
		
		public function clearPopUpClass():void
		{
			trace("Setting Popup Class to Null");
			popUpClassProp = null;
		}
	}
}