package controller
{
	import event.FireEvent;
	import event.NavigationEvent;
	
	import flash.events.EventDispatcher;
	
	import view.popup.MyPopUp;

	public class MainController
	{
		[Dispatcher] public var eventDispatcher:EventDispatcher;
		
		[Mediate(event="FireEvent.FIRE_EVENT")]
		public function controllerPickup(fireEvent:FireEvent):void
		{
			trace("Picked up event in Controller");
		
			var navigationEvent:NavigationEvent = new NavigationEvent(NavigationEvent.OPEN_POPUP);
			
			trace("Dispatching Event to popup Window");
			eventDispatcher.dispatchEvent(navigationEvent);
		}
	}
}