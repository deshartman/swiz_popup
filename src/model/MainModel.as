package model
{
	    public class MainModel
	    {
			[Bindable]
			public var message:String = "Hello from the model";
			
			[Bindable]
			public var showAgain:Boolean;
			
			[Bindable]
			public var acceptedConditions:Boolean;
			
			public function MainModel()
			{
				showAgain = true;
				acceptedConditions = true;
			}
			
			
		
	    }
	
}